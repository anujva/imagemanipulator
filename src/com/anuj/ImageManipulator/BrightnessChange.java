package com.anuj.ImageManipulator;

import java.io.File;
import java.io.FileOutputStream;

import com.anuj.ImageManipulator.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

public class BrightnessChange extends Activity{
	private ImageView imageview;
	private Button button;
	private SeekBar seekbar1;
	private SeekBar seekbar2;
	private SeekBar seekbar3;
	static private Bitmap photo;
	private ColorMatrix cm;
	private Canvas canvas;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.brightnesslayout);
	    // TODO Auto-generated method stub
	    imageview = (ImageView)findViewById(R.id.imageView11);
	    seekbar1 = (SeekBar)findViewById(R.id.seekBar1);
	    seekbar2 = (SeekBar)findViewById(R.id.seekBar2);
	    seekbar3 = (SeekBar)findViewById(R.id.seekBar3);
	    button = (Button)findViewById(R.id.button11);
	    button.setVisibility(Button.INVISIBLE);
	    
	    seekbar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
	    	int p=0;
	    	Bitmap temp;
	    	
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				 
	            try{
	            	File root_file = Environment.getExternalStorageDirectory();
	            	if(root_file.canWrite()){
	            		File image_photo1 = new File(root_file, "image3.jpg");
	            		FileOutputStream out = new FileOutputStream(image_photo1);
	            		temp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	            		
	            		out.flush();
	            		out.close();
	            	}
	            }catch(Exception e){
	            	
	            }
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				p = progress;
				System.out.println(p);
				Paint paint = new Paint();
				cm = new ColorMatrix();
				
				float contrast  = (float) (p*0.02f - 1);
				setContrastScaleOnly(cm, contrast);
	            paint.setColorFilter(new ColorMatrixColorFilter(cm));
	            temp = photo.copy(photo.getConfig(), true);
	            canvas = new Canvas(temp);
	            canvas.drawBitmap(temp, 0, 0, paint);
	            
	            imageview.setImageBitmap(temp);
			}
		});
	    
	    seekbar2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
	    	
	    	int p=0;
	    	Bitmap temp;
	    	
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
	            
	            try{
	            	File root_file = Environment.getExternalStorageDirectory();
	            	if(root_file.canWrite()){
	            		File image_photo1 = new File(root_file, "image3.jpg");
	            		FileOutputStream out = new FileOutputStream(image_photo1);
	            		temp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	            		
	            		out.flush();
	            		out.close();
	            	}
	            }catch(Exception e){
	            	
	            }
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				p = progress;
				System.out.println(p);
				Paint paint = new Paint();
				cm = new ColorMatrix();
				
				float contrast  = (float) (p*0.02f - 1);
				setContrast(cm, contrast);
	            paint.setColorFilter(new ColorMatrixColorFilter(cm));
	            temp = photo.copy(photo.getConfig(), true);
	            canvas = new Canvas(temp);
	            canvas.drawBitmap(temp, 0, 0, paint);
	            
	            imageview.setImageBitmap(temp);
			}
		});
	    
	    seekbar3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
	    	int p=0;
	    	Bitmap temp;
	    	
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
	            try{
	            	File root_file = Environment.getExternalStorageDirectory();
	            	if(root_file.canWrite()){
	            		File image_photo1 = new File(root_file, "image3.jpg");
	            		FileOutputStream out = new FileOutputStream(image_photo1);
	            		temp.compress(Bitmap.CompressFormat.JPEG, 100, out);
	            		
	            		out.flush();
	            		out.close();
	            	}
	            }catch(Exception e){
	            	
	            }
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				p = progress;
				System.out.println(p);
				Paint paint = new Paint();
				cm = new ColorMatrix();
				
				float contrast  = (float) (p*0.02f - 1);
				setContrastTranslateOnly(cm, contrast);
	            paint.setColorFilter(new ColorMatrixColorFilter(cm));
	            temp = photo.copy(photo.getConfig(), true);
	            canvas = new Canvas(temp);
	            canvas.drawBitmap(temp, 0, 0, paint);
	            
	            imageview.setImageBitmap(temp);
			}
		});
	    
	    try{
        	File root_file = Environment.getExternalStorageDirectory();
        	System.out.println("reached here");
        	Toast.makeText(getApplicationContext(), "Root File"+root_file.canRead(), Toast.LENGTH_LONG).show();
        	if(root_file.canRead()){
        		File image_photo = new File(root_file, "image.jpg");
        		photo = BitmapFactory.decodeFile(image_photo.getPath());
        		if(photo == null){
        			System.out.println("photo was null");
        		}
        		System.out.println(photo.toString());
        		System.out.println("reached there");
        		
        		File image_photo1 = new File(root_file, "image3.jpg");
        		FileOutputStream out = new FileOutputStream(image_photo1);
        		
        		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		
        		out.flush();
        		out.close();
        	}
        }catch(Exception e){
        	System.out.println("Was not able to read the file");
        }
	    try{
	    	imageview.setImageBitmap(photo);
	    }catch(Exception e){
	    	System.out.println("Image cannot be read");
	    }
	    
	    photo = photo.copy(photo.getConfig(), true);
	}
	
	private static void setContrastScaleOnly(ColorMatrix cm, float contrast) {
        float scale = contrast + 1.f;
        cm.set(new float[] {
               scale, 0, 0, 0, 0,
               0, scale, 0, 0, 0,
               0, 0, scale, 0, 0,
               0, 0, 0, 1, 0 });
    }
   private static void setContrast(ColorMatrix cm, float contrast) {
        float scale = contrast + 1.f;
           float translate = (-.5f * scale + .5f) * 255.f;
        cm.set(new float[] {
               scale, 0, 0, 0, translate,
               0, scale, 0, 0, translate,
               0, 0, scale, 0, translate,
               0, 0, 0, 1, 0 });
    }

    private static void setContrastTranslateOnly(ColorMatrix cm, float contrast) {
        float scale = contrast + 1.f;
           float translate = (-.5f * scale + .5f) * 255.f;
        cm.set(new float[] {
               1, 0, 0, 0, translate,
               0, 1, 0, 0, translate,
               0, 0, 1, 0, translate,
               0, 0, 0, 1, 0 });
    }
	
}
