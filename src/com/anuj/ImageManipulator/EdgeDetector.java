package com.anuj.ImageManipulator;

import java.io.File;
import java.io.FileOutputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class EdgeDetector extends Activity {
	private Bitmap photo;
	private ImageView imageView;
	private Button button;
	private ProgressDialog pdia;
	
private class LongOperation extends AsyncTask<Bitmap, Integer, Boolean> {
    	
    	boolean success;
    	@Override
        protected Boolean doInBackground(Bitmap... photo2) {
    		Bitmap photo3[] = new Bitmap[1];
        	int index=0;
        	for ( Bitmap d : photo2 ){
        		photo3[index++] = d;
        	}
        	photo = photo3[0];
        	CannyEdgeDetector ceDetector = new CannyEdgeDetector();
    	    ceDetector.setSourceImage(photo);
    	    ceDetector.process();
    	    photo = ceDetector.getEdgesImage();
    	    try{
            	File root_file = Environment.getExternalStorageDirectory();
            	if(root_file.canWrite()){
            		File image_photo1 = new File(root_file, "imageEdge.jpg");
            		FileOutputStream out = new FileOutputStream(image_photo1);
            		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            		
            		out.flush();
            		out.close();
            	}
            }catch(Exception e){
            	
            }
    		return success;
        }      

        @Override
        protected void onPostExecute(Boolean result) {
        	imageView.setVisibility(ImageView.VISIBLE);
        	imageView.setImageBitmap(photo);
        	closeDialog();
        }

        @Override
        protected void onPreExecute() {
        	showDialog();
        }

        @Override
        protected void onProgressUpdate(Integer... values){
        	
        }
    }   
    
    public void showDialog(){
    	Activity a = this;
    	pdia = new ProgressDialog(a);
    	pdia.setMessage("Doing computation");
    	pdia.show();
    }
    
    public void closeDialog(){
    	try{
    		pdia.dismiss();
    		pdia = null;
    	}catch(Exception e){
    		//do nothing
    	}
    }
    
    
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.edgedetectorlayout);
	    // TODO Auto-generated method stub
	    imageView = (ImageView)findViewById(R.id.imageview31);
	    
	    try{
        	File root_file = Environment.getExternalStorageDirectory();
        	Toast.makeText(getApplicationContext(), "Root File"+root_file.canRead(), Toast.LENGTH_LONG).show();
        	if(root_file.canRead()){
        		File image_photo = new File(root_file, "image.jpg");
        		photo = BitmapFactory.decodeFile(image_photo.getPath());
        		if(photo == null){
        			System.out.println("photo was null");
        		}
        		System.out.println(photo.toString());
        		System.out.println("reached there");
        		
        		File image_photo1 = new File(root_file, "image4.jpg");
        		FileOutputStream out = new FileOutputStream(image_photo1);
        		
        		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		
        		out.flush();
        		out.close();
        	}
        }catch(Exception e){
        	System.out.println("Was not able to read the file");
        }
	    imageView.setImageBitmap(photo);
	    
	    button = (Button)findViewById(R.id.button31);
	    
	    button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new LongOperation().execute(photo);
            }
        });
	}

}
