package com.anuj.ImageManipulator;

import java.io.File;
import java.io.FileOutputStream;

import com.anuj.ImageManipulator.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity {
    private static final int CAMERA_REQUEST = 1888; 
    private ImageView imageView;
    private ImageView imageView1;
    private ImageView imageView2;
    private Button button;
    private Button photoButton;
    private Button SeamCarvingButton;
    Bitmap photo;
    Bitmap photo1;
    private ProgressDialog pdia;
    private Button brightnessButton;
    private Button doEdgeDetector;
    
    private class LongOperation extends AsyncTask<Bitmap, Integer, Boolean> {
    	
    	boolean success;
    	Bitmap photo, photo1;
    	@Override
        protected Boolean doInBackground(Bitmap... photo2) {
        	
        	Bitmap photo3[] = new Bitmap[2];
        	int index=0;
        	for ( Bitmap d : photo2 ){
        		photo3[index++] = d;
        	}
        	photo1 = photo3[0];
        	photo = photo3[1];
        	
        	TwoBitmaps twobit = Oilfilter(photo1, photo);
        	photo = twobit.photo4;
        	photo1 = twobit.photo5;
        	success = true;
			return success;
        }      

        @Override
        protected void onPostExecute(Boolean result) {
        	imageView.setLayoutParams(new LinearLayout.LayoutParams(photo1.getWidth(), photo1.getHeight()));
        	imageView.setImageBitmap(photo1);
        	
        	imageView1.setVisibility(ImageView.INVISIBLE);
        	/*if(result){
        		Toast.makeText(getApplicationContext(),
        	               "Image successfully saved", Toast.LENGTH_LONG).show();
        	}else{
        		Toast.makeText(getApplicationContext(),
        	               "Image saving failed", Toast.LENGTH_LONG).show();
        	}*/
        	closeDialog();
        }

        @Override
        protected void onPreExecute() {
        	showDialog();
        }

        @Override
        protected void onProgressUpdate(Integer... values){
        	
        }
    }   
    
    public void showDialog(){
    	Activity a = this;
    	pdia = new ProgressDialog(a);
    	pdia.setMessage("Doing computation");
    	pdia.show();
    }
    
    public void closeDialog(){
    	try{
    		pdia.dismiss();
    		pdia = null;
    	}catch(Exception e){
    		//do nothing
    	}
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.imageView = (ImageView)this.findViewById(R.id.imageView1);
        this.imageView1 = (ImageView)this.findViewById(R.id.imageView2);
        this.imageView2 = (ImageView)this.findViewById(R.id.imageView3);
        photoButton = (Button) this.findViewById(R.id.button1);
        button = (Button)this.findViewById(R.id.button2);
        doEdgeDetector = (Button)findViewById(R.id.button5);
        doEdgeDetector.setVisibility(Button.INVISIBLE);
        photoButton.setText("Take a pic");
        button.setText("Run the Oil Painting Code");
        brightnessButton = (Button)findViewById(R.id.button3);
        SeamCarvingButton = (Button)findViewById(R.id.button4);
        SeamCarvingButton.setVisibility(Button.INVISIBLE);
        imageView.setVisibility(ImageView.INVISIBLE);
        imageView1.setVisibility(ImageView.INVISIBLE);
        imageView2.setVisibility(ImageView.INVISIBLE);
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
            }
        });
        
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new LongOperation().execute(photo1, photo);
            }
        });
        
        button.setVisibility(Button.INVISIBLE);
        brightnessButton.setVisibility(Button.INVISIBLE);
        
        brightnessButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(), BrightnessChange.class);
				startActivity(intent);
			}
		});
        
        doEdgeDetector.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(), EdgeDetector.class);
				startActivity(intent);
			}
		});
        
        SeamCarvingButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent =new Intent(getApplicationContext(), SeamCarving.class);
				startActivity(intent);
			}
		});
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
            photo = (Bitmap) data.getExtras().get("data"); 
            photo1 = photo.copy(photo.getConfig(), true);
            
            imageView.setVisibility(ImageView.VISIBLE);
            imageView.setImageBitmap(photo);
            
            photoButton.setText("Take another Picture");
            button.setVisibility(Button.VISIBLE);
            brightnessButton.setVisibility(Button.VISIBLE);
            doEdgeDetector.setVisibility(Button.VISIBLE);
            SeamCarvingButton.setVisibility(Button.VISIBLE);
            
            try{
            	File root_file = Environment.getExternalStorageDirectory();
            	if(root_file.canWrite()){
            		File image_photo1 = new File(root_file, "image.jpg");
            		FileOutputStream out = new FileOutputStream(image_photo1);
            		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            		
            		out.flush();
            		out.close();
            	}
            }catch(Exception e){
            	
            }
            
        }else{
        	Toast.makeText(getApplicationContext(), "Could not capture the image properly.. Please try again", Toast.LENGTH_LONG).show();
        }
    }
    
    private class TwoBitmaps{
    	private Bitmap photo4;
    	private Bitmap photo5;
    	
    	public TwoBitmaps(Bitmap pho, Bitmap pho1){
    		photo4 = pho;
    		photo5 = pho1;
    	}
    };
    
    public TwoBitmaps Oilfilter(Bitmap photo1, Bitmap photo){
    	int width_photo = photo1.getWidth();
        int height_photo = photo1.getHeight();
        
    	for(int i=0; i<height_photo; i++){
        	for(int j=0; j<width_photo; j++){
        		
        		int averager[] = new int[256], averageg[] = new int[256], averageb[] = new int[256];
        		int intensityCount[] = new int[256];
        		for(int k=-2; k<3; k++){
        			for(int l=-2; l<3; l++){
        				int x=0, y=0;
        				if(i+k<0){
        					y= -1*(i+k)-1;
        				}else if(i+k>height_photo-1){
        					y = height_photo-(i+k-height_photo+1);
        				}else{
        					y = i+k;
        				}
        				
        				if(j+l<0){
        					x= -1*(j+l)-1;
        				}else if(j+l>width_photo-1){
        					x = width_photo-(j+l-width_photo+1);
        				}else{
        					x = j+l;
        				}
        				
        				int color = photo.getPixel(x, y);
                		int red = Color.red(color);
                		int green = Color.green(color);
                		int blue = Color.blue(color);	
                		//System.out.println("red: "+red);
                		//System.out.println("green: "+green);
                		//System.out.println("blue: "+blue);
                		int currentIntensity = (int)((double)((red+green+blue)/3)*20/255.0f);
                		//System.out.println("currentIntensity: "+currentIntensity);
                		intensityCount[currentIntensity]++;
                		//System.out.println("intensityCount at currentIntensity: "+intensityCount[currentIntensity]);
                		averager[currentIntensity] += red;
                		averageb[currentIntensity] += blue;
                		averageg[currentIntensity] += green;
        			}
        		}
        		int currMax = intensityCount[0];
        		//System.out.println("currentMax: "+currMax);
        		int maxIndex = 0;
        		for(int m=0; m<256; m++){
        			if(intensityCount[m]>currMax){
        				currMax = intensityCount[m];
        				maxIndex = m;
        			}
        		}
        		//System.out.println("currentMax: "+currMax);
        		int finalR = averager[maxIndex] / currMax;
        		int finalG = averageg[maxIndex] / currMax;
        		int finalB = averageb[maxIndex] / currMax;
        		
        		int color = Color.argb(255, finalR, finalG, finalB);
        		photo1.setPixel(j, i, color);
        	}
        }
    	
        //save the image to disk
        try{
        	File root_file = Environment.getExternalStorageDirectory();
        	if(root_file.canWrite()){
        		File image_photo1 = new File(root_file, "image1.jpg");
        		FileOutputStream out = new FileOutputStream(image_photo1);
        		photo1.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		
        		out.flush();
        		out.close();
        	}
        }catch(Exception e){
        	
        }
        return new TwoBitmaps(photo, photo1);
    }
}
