package com.anuj.ImageManipulator;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import com.anuj.ImageManipulator.R;

import kanzi.filter.seam.ContextResizer;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SeamCarving extends Activity {
	
	private Bitmap photo;
	private final static int MINPERCENT = 10;
    private final static int MAXPERCENT = 90;
    protected static final String TAG = "Error";

    private int pictureResize = 0;
    private boolean debug = false;
    private boolean realtime = false;

    private boolean verti_bool = false;
    private boolean hori_bool = false;
    
    
    RadioGroup groupDir;
    RadioButton radioVert;
    RadioButton radioHori;
    RadioButton radioBoth;
    TextView textPctResize;
    ImageView targetImage;
    SeekBar pctBar;
    CheckBox debugCheck;
    CheckBox realTimeCheck;
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    // TODO Auto-generated method stub
	    setContentView(R.layout.seamcarvinglayout);
	    try{
        	File root_file = Environment.getExternalStorageDirectory();
        	//Toast.makeText(getApplicationContext(), "Root File"+root_file.canRead(), Toast.LENGTH_LONG).show();
        	if(root_file.canRead()){
        		File image_photo = new File(root_file, "image.jpg");
        		photo = BitmapFactory.decodeFile(image_photo.getPath());
        		if(photo == null){
        			System.out.println("photo was null");
        		}
        		System.out.println(photo.toString());
        		System.out.println("reached there");
        		
        		File image_photo1 = new File(root_file, "image4.jpg");
        		FileOutputStream out = new FileOutputStream(image_photo1);
        		
        		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		
        		out.flush();
        		out.close();
        	}
        }catch(Exception e){
        	System.out.println("Was not able to read the file");
        }
	    
	    
	    
        radioVert = (RadioButton) findViewById(R.id.radio_vert);
        radioHori = (RadioButton) findViewById(R.id.radio_hori);
        radioBoth = (RadioButton) findViewById(R.id.radio_both);
        targetImage = (ImageView) findViewById(R.id.target_image);
	    textPctResize = (TextView) findViewById(R.id.pct_elab_text);
	    debugCheck = (CheckBox) findViewById(R.id.dbg_mode_cb);
        realTimeCheck = (CheckBox) findViewById(R.id.rt_mode_cb);
        pctBar = (SeekBar) findViewById(R.id.pct_elab_bar);
        groupDir = (RadioGroup) findViewById(R.id.group_dir);

        pctBar.setMax(MAXPERCENT - MINPERCENT);
        debugCheck.setChecked(debug);
        realTimeCheck.setChecked(realtime);
        radioVert.setChecked(false);
        radioHori.setChecked(false);
        radioBoth.setChecked(false);
        targetImage.getLayoutParams().height = photo.getHeight();
        targetImage.getLayoutParams().width = photo.getWidth();
        targetImage.setImageBitmap(photo);
        groupDir.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                    case -1:
                            Log.v(TAG, "Huh?");
                            break;
                    case R.id.radio_vert:
                            Log.v(TAG, "vertical");
                            verti_bool = true;
                            hori_bool = false;
                            radioVert.setChecked(true);
                            radioHori.setChecked(false);
                            radioBoth.setChecked(false);
                            break;
                    case R.id.radio_hori:
                            Log.v(TAG, "horizontal");
                            verti_bool = false;
                            hori_bool = true;
                            radioVert.setChecked(false);
                            radioHori.setChecked(true);
                            radioBoth.setChecked(false);
                            break;
                    case R.id.radio_both:
                            Log.v(TAG, "both");
                            verti_bool = true;
                            hori_bool = true;
                            radioVert.setChecked(false);
                            radioHori.setChecked(false);
                            radioBoth.setChecked(true);
                            break;
                    default:
                            Log.v(TAG, "Huh?");
                            break;
                    }
            }
    });
    
    debugCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                            boolean isChecked) {
                    debug = isChecked;
            }
    });
    
    realTimeCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                            boolean isChecked) {
                    realtime = isChecked;
            }
    });

    pctBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                            boolean fromUser) {
                    pictureResize = MINPERCENT + progress;
                    textPctResize.setText("The amount of Resize is given by: " + pictureResize + "%");
                    
                    //realtime
                    if(realtime){
                            if (photo == null) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Please load an image!", Toast.LENGTH_SHORT).show();
                            } else if (pictureResize == 0) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Percentage is 0!", Toast.LENGTH_SHORT).show();
                            } else if (!verti_bool && !hori_bool) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Choose a Direction!", Toast.LENGTH_SHORT).show();
                            } else {
                                    //Toast.makeText(getApplicationContext(), "Elaborazione..", Toast.LENGTH_SHORT).show();
                                    Bitmap bitmap = contentAwareResize(photo);
                                    targetImage.getLayoutParams().height = bitmap.getHeight();
                                    targetImage.getLayoutParams().width = bitmap.getWidth();
                                    targetImage.setImageBitmap(bitmap);
                                    try{
                                    	File root_file = Environment.getExternalStorageDirectory();
                                    	if(root_file.canWrite()){
                                    		File image_photo1 = new File(root_file, "image_seam.jpg");
                                    		FileOutputStream out = new FileOutputStream(image_photo1);
                                    		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                    		
                                    		out.flush();
                                    		out.close();
                                    	}
                                    }catch(Exception e){
                                    	
                                    }
                            }
                    }
            }
    });

	}
	
	protected Bitmap contentAwareResize(Bitmap bitmap) {

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        int[] source = new int[w * h];
        int[] destination = new int[w * h];
        bitmap = bitmap.copy(Config.RGB_565, false);
       
        bitmap.getPixels(source, 0, w, 0, 0, w, h);

        ContextResizer resizer = null;

        Arrays.fill(destination, 0);

        //int newW = w;
        int newHeight = h;
        int diffWidth = 0;
        int diffHeight = 0;

        if (verti_bool == true) {
                diffWidth = w * pictureResize / 100;
                //newW = w - difW;
        }
        if (hori_bool == true) {
                diffHeight = h * pictureResize / 100;
                newHeight = h - diffHeight;
        }
        
        if (verti_bool && !hori_bool) {
                resizer = new ContextResizer(w, h, 0, w, ContextResizer.VERTICAL,
                                ContextResizer.SHRINK, w, diffWidth);
        }
        if (hori_bool) {
                if (verti_bool) {
                        resizer = new ContextResizer(w, h, 0, w,
                                        ContextResizer.VERTICAL, ContextResizer.SHRINK, w, diffWidth);
                        resizer.setDebug(debug);
                        resizer.apply(source, destination);
                        source = destination;
                        resizer = null;
                        resizer = new ContextResizer(w, h, 0, w,
                                        ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                        diffHeight);
                } else {
                        resizer = new ContextResizer(w, h, 0, w,
                                        ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                        diffHeight);
                }
        }
        resizer.setDebug(debug);
        resizer.apply(source, destination);
        
        if (hori_bool && verti_bool && !debug) {
                int goodPixel = newHeight;
                for (int i = (goodPixel * w); i < destination.length; i++) {
                        destination[i] = 0;
                }
        }
        Bitmap resizedBitmap = Bitmap.createBitmap(destination, w, h, Config.RGB_565);

        return resizedBitmap;
	}

}
